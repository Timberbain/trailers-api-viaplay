# Assignment API - Viaplay
Api service for fetching trailers based on Viaplay content url.

## Dependencies
- [Docker](https://www.docker.com/)
- [NodeJS](https://nodejs.org)

## Setup
** Install dependencies for local development **
```
cd server/app
npm install
```
Note: The node_modules are installed when the docker image is built, but it can be smooth to have the node_modules installed locally as well when doing development and required when running tests.

** Run tests **
```
cd server/app
npm test
```

** Run application using docker **
```
docker-compose up
```
The `docker-compose.yaml` is configured to use volumes, this is for improving the local development process when running the node service using `nodemon`.

## Calling the API
The application exposes two endpoint, one is destined to be used for making health checks.
The other returns a link to a trailer based on the input url.

#### Health Check

Endpoint: `GET localhost:3000/health`

Example Request:
```
curl --request GET "localhost:3000/health"
```
Example Response:
```
{
  "result": {
    "api": "ready",
    "cache": "ready"
  }
}
```

#### Trailers

Endpoint: `GET localhost:3000/trailers`

Params:
```
  url: String
```

Example Request:
```
curl --location --request GET "localhost:3000/trailers?url=https://content.viaplay.se/pc-se/film/wilson-2017"
```
Example Response:
```
{
  "result": {
    "imdb_id": "tt1781058",
    "trailer_url": "https://www.youtube.com/watch?v=7Hf305QmNXc"
  }
}
```


## Notes

#### Input Validation
The input validation for the trailer endpoint only checks if the input is a url string and belongs to the content viaplay api subdomain. The reason for this is that if the url of the films would change (but still within the content api) the application doesn't have to be updated.

#### Cacheing
When the service calls the Viaplay content api and TMDB api, a redis database is used for cacheing the response.

#### Authentication
Due to the fact that authentication was not in the scope of the assignment, is has been left out when calling the API.
