const ServiceError = require('../util/error');

class HealthError extends ServiceError {}

const cache = require('../services/jsoncache');

module.exports = {
  getHealth() {
    try {
      return {
        api: 'ready',
        cache: cache.health(),
      };
    } catch (e) {
      if (e instanceof ServiceError) throw e;
      throw new HealthError('An error occured when doing helth check.');
    }
  },
};
