const ServiceError = require('../util/error');

class TrailerError extends ServiceError {}

const tmdb = require('../services/tmdb');
const content = require('../services/content');
const { createYoutubeLink } = require('../services/youtube');

module.exports = {
  async getTrailer(contentUrl) {
    try {
      const imdb = await content.getFilmImdb(contentUrl);
      const videos = await tmdb.getMovieVideosById(imdb.id);
      const trailer = videos.results.find(v => v.type === 'Trailer' && v.site === 'YouTube');
      if (!trailer || typeof trailer.key !== 'string') {
        throw new TrailerError('Unable to find trailer', 404);
      }
      return {
        imdb_id: imdb.id,
        trailer_url: createYoutubeLink(trailer.key),
      };
    } catch (e) {
      if (e instanceof ServiceError) throw e;
      throw new TrailerError(`An error occured when fetching trailer based on url: ${contentUrl}`);
    }
  },
};
