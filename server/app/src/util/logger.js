const { createLogger, transports, format } = require('winston');

const {
  combine,
  timestamp,
  prettyPrint,
} = format;

const log = createLogger({
  level: process.env.LOG_LEVEL || 'info',
  format: combine(
    timestamp({
      format: 'YYYY-MM-DD HH:mm:ss',
    }),
    prettyPrint(),
  ),
  transports: [
    new transports.Console(),
  ],
});

// create a stream object with a 'write' function that will be used by `morgan`
log.stream = {
  write: message => log.info(message),
};


module.exports = {
  log,
};
