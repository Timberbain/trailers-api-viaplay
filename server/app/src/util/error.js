class ServiceError extends Error {
  constructor(message, code, details) {
    super(message);
    this.name = this.constructor.name;
    Error.captureStackTrace(this, this.constructor);
    this.code = code;
    this.details = details;
  }
}
module.exports = ServiceError;
