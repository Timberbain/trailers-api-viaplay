module.exports = {
  isset: v => v !== null && typeof v !== 'undefined',
};
