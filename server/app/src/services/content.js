const request = require('request-promise');

const cache = require('./jsoncache');

const ServiceError = require('../util/error');

class ContentError extends ServiceError {}

function handleApiError(err) {
  switch (err.statusCode) {
    case 404: return 'Movie Not Found';
    default: return 'An error occurred when accessing the movie DB api';
  }
}

function extractImdbContent(movieResult) {
  try {
    // eslint-disable-next-line
    const { imdb } = movieResult._embedded['viaplay:blocks'][0]._embedded['viaplay:product'].content;
    return imdb;
  } catch (err) {
    log.error(err.message);
    throw new ContentError('Unable to extract imdb data from movie result');
  }
}

async function getFilmImdb(movieResourceLink) {
  try {
    try {
      const cachedResult = await cache.get(movieResourceLink);
      if (cachedResult) return cachedResult;
    } catch (e) {
      log.warn('Unable to get from cache');
    }

    let result;
    try {
      result = await request({
        uri: `${movieResourceLink}`,
        method: 'GET',
        json: true,
      });
      log.debug(result);
    } catch (err) {
      log.error(err.message);
      throw new ContentError(handleApiError(err), err.statusCode || 500);
    }
    const imdbContent = extractImdbContent(result);
    try {
      return cache.set(movieResourceLink, imdbContent, 10);
    } catch (e) {
      log.warn('Unable to set cache');
    }
    return imdbContent;
  } catch (err) {
    if (err instanceof ServiceError) throw err;
    log.error(err.message);
    throw new ContentError('Unable to fetch film resource from content api');
  }
}

module.exports = {
  getFilmImdb,
};
