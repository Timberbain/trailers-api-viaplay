module.exports = {
  createYoutubeLink(youtubeKey) {
    return `https://www.youtube.com/watch?v=${youtubeKey}`;
  },
};
