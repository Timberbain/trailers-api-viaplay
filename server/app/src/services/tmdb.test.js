const { createLogger, transports } = require('winston');

global.log = createLogger({ transports: [new transports.Console({ silent: true })] });

process.env.TMDB_APIKEY = '7e5b964a2605db576194821036cbe552';

const tmdb = require('./tmdb');

describe('The Movide db - Api wrapper', () => {
  describe('getMovieVideosById()', () => {
    it('should return valid response', async () => {
      const result = await tmdb.getMovieVideosById('tt1781058');
      expect(result).not.toBe(null);
    });

    it('should throw not found', async () => {
      await expect(tmdb.getMovieVideosById('tt1781058-5')).rejects.toThrow(Error);
    });

    it('should return valid json', async () => {
      const result = await tmdb.getMovieVideosById('tt1781058');
      expect(result).toHaveProperty('id');
      expect(result).toHaveProperty('results');
      expect(Array.isArray(result.results)).toBe(true);
      result.results.map((r) => {
        expect(r).toHaveProperty('id');
        expect(typeof r.id).toBe('string');
        expect(r).toHaveProperty('iso_639_1');
        expect(typeof r.iso_639_1).toBe('string');
        expect(r).toHaveProperty('iso_3166_1');
        expect(typeof r.iso_3166_1).toBe('string');
        expect(r).toHaveProperty('key');
        expect(typeof r.key).toBe('string');
        expect(r).toHaveProperty('name');
        expect(typeof r.name).toBe('string');
        expect(r).toHaveProperty('site');
        expect(typeof r.site).toBe('string');
        expect(r).toHaveProperty('size');
        expect(typeof r.size).toBe('number');
        expect(r).toHaveProperty('type');
        expect(typeof r.type).toBe('string');
        return 0;
      });
    });
  });
});
