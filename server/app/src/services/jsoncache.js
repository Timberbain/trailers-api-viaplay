const bluebird = require('bluebird');
const redis = require('redis');

const ServiceError = require('../util/error');

class CacheError extends ServiceError {}

bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

let status = 'not_ready';
const client = redis.createClient({
  host: process.env.REDIS_HOST || 'localhost',
  port: process.env.REDIS_PORT || 6397,
  password: process.env.REDIS_PASSWORD,
})
  .on('ready', () => { status = 'ready'; })
  .on('error', () => { status = 'connection_error'; });


module.exports = {
  health() {
    return status;
  },

  set(key, val, exp = 20) {
    try {
      const valStr = JSON.stringify(val);
      if (status !== 'ready') {
        throw new CacheError('Unable to set cache, db not ready');
      }
      client.setex(key, exp, valStr);
      return val;
    } catch (err) {
      throw err;
    }
  },

  async get(key) {
    try {
      if (status !== 'ready') {
        throw new CacheError('Unable to get from cache, db not ready');
      }
      const valStr = await client.getAsync(key);
      return JSON.parse(valStr);
    } catch (err) {
      throw err;
    }
  },
};
