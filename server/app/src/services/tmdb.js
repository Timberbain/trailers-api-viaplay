const request = require('request-promise');

const cache = require('./jsoncache');

const ServiceError = require('../util/error');

class TMDBError extends ServiceError {}

const API_KEY = process.env.TMDB_APIKEY || '';
const API_VERSION = '3';
const BASE_URL = 'https://api.themoviedb.org';

function handleApiError(err) {
  switch (err.statusCode) {
    case 401: return 'Unathorized';
    case 404: return 'Not Found';
    default: return 'An error occurred when accessing the movie DB api';
  }
}

module.exports = {
  async getMovieVideosById(movieId) {
    try {
      try {
        const cachedResult = await cache.get(movieId);
        if (cachedResult) return cachedResult;
      } catch (e) {
        log.warn('Unable to get from cache');
      }
      log.debug('fetching video results');
      const videoResults = await request({
        uri: `${BASE_URL}/${API_VERSION}/movie/${movieId}/videos`,
        method: 'GET',
        qs: {
          api_key: API_KEY,
        },
        useQuerystring: true,
        json: true,
      });

      try {
        return cache.set(movieId, videoResults, 10);
      } catch (e) {
        log.warn('Unable to set cache');
      }
      return videoResults;
    } catch (err) {
      log.error(err.message);
      throw new TMDBError(handleApiError(err), err.statusCode || 500);
    }
  },
};
