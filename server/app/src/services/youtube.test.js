const youtube = require('./youtube');

describe('YouTube wrapper', () => {
  describe('createYoutubeLink()', () => {
    it('should generate a valid url', async () => {
      const key = '1234567';
      expect(youtube.createYoutubeLink(key)).toBe(`https://www.youtube.com/watch?v=${key}`);
    });
  });
});
