const { createLogger, transports } = require('winston');

global.log = createLogger({ transports: [new transports.Console({ silent: true })] });

const {
  getFilmImdb,
  // extractImdbContent,
} = require('./content');

const movieUrl = 'https://content.viaplay.se/pc-se/film/wilson-2017';

describe('Viaplay - content api wrapper', () => {
  describe('getFilmImdb()', () => {
    it('should return valid response', async () => {
      const result = await getFilmImdb(movieUrl);
      expect(result).not.toBe(null);
    });

    it('should throw not found', async () => {
      await expect(getFilmImdb(`${movieUrl}-not-a-movie`)).rejects.toThrow(Error);
    });

    it('should return valid json', async () => {
      const result = await getFilmImdb(movieUrl);
      expect(result).toHaveProperty('id');
      expect(typeof result.id).toBe('string');
      expect(result).toHaveProperty('rating');
      expect(typeof result.rating).toBe('string');
      expect(result).toHaveProperty('votes');
      expect(typeof result.votes).toBe('string');
      expect(result).toHaveProperty('url');
      expect(typeof result.url).toBe('string');
    });
  });
});
