const express = require('express');

const c = require('../middlewares/controller_handler');

const { getHealth } = require('../controllers/health');

const router = express.Router();

router.get('/', c(getHealth));

module.exports = router;
