const express = require('express');

const router = express.Router();

const { celebrate, Joi } = require('celebrate');

const c = require('../middlewares/controller_handler');

const { getTrailer } = require('../controllers/trailers');

router.get('/',
  celebrate({
    query: {
      url: Joi.string().uri().regex(/^https?:\/\/content\.viaplay\.se\/.*$/).required(),
    },
  }),
  c(getTrailer, req => [
    req.query.url,
  ]));

module.exports = router;
