const express = require('express');

const { isCelebrate } = require('celebrate');

const { addToRouterInFolder } = require('../util/routes');

const router = express.Router();

addToRouterInFolder('/', router, __dirname);

router.use((err, req, res, next) => {
  if (isCelebrate(err)) {
    res.status(400);
    next(new Error(err.joi.message));
  }
  next(err);
});

router.use((req, res, next) => {
  res.status(404);
  next(new Error('Api route not found'));
});

module.exports = router;
