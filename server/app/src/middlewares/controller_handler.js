module.exports = (promise, params) => async (req, res, next) => {
  try {
    const boundParams = params ? params(req, res, next) : [];
    const result = await promise(...boundParams);
    return res.send({ result });
  } catch (err) {
    res.status(err.code || 500);
    return next(err);
  }
};
