require('./globals');

const express = require('express');

const morgan = require('morgan');

const helmet = require('helmet');

const api = require('./routes');

const app = express();

app.use(helmet());

app.use(morgan('combined', { stream: log.stream }));

app.use('/', api);

app.use((err, req, res, next) => { // eslint-disable-line
  let error = {};
  if (res.statusCode) {
    error = {
      message: err.message,
    };
  } else {
    res.status(500);
    error = {
      message: 'An error has occurred, please contact system administrator for more information',
    };
  }
  res.json({ error });
});
const port = process.env.PORT || 3000;

app.listen(port, () => log.info(`Trailer API running, listening on port ${port}`));
